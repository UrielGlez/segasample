package mx.uach.segamobile

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import mx.uach.segamobile.adapters.GradeAdapter
import mx.uach.segamobile.models.Grade

/**
 * A simple [Fragment] subclass.
 * Use the [GradeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class GradeFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_grade, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val json: String = "[{\"Calificacion\":\"10.00\",\"Materia\":\"OPC02\",\"Evaluacion\":\"PARCIAL 1\",\"RegistraCalificacion\":1,\"Fecha\":\"29/03/2021\",\"Descripcion\":\"WEB PLATFORMS\",\"SituacionMateria\":\"AC\",\"Opcion\":\"\",\"Ciclo\":\"07\",\"Tipo\":\"T\"},{\"Calificacion\":\"10.00\",\"Materia\":\"CI871\",\"Evaluacion\":\"PARCIAL 1\",\"RegistraCalificacion\":1,\"Fecha\":\"13/04/2021\",\"SituacionMateria\":\"AC\",\"Descripcion\":\"COMPUTO PARALELO Y DISTRIBUIDO\",\"Opcion\":\"\",\"Ciclo\":\"08\",\"Tipo\":\"T\"},{\"Calificacion\":\"10.00\",\"Materia\":\"CI872\",\"Evaluacion\":\"PARCIAL 1\",\"RegistraCalificacion\":1,\"Fecha\":\"10/03/2021\",\"SituacionMateria\":\"AC\",\"Descripcion\":\"GRAFICACION\",\"Opcion\":\"\",\"Ciclo\":\"08\",\"Tipo\":\"T\"},{\"Calificacion\":\"9.80\",\"Materia\":\"CI877\",\"Evaluacion\":\"PARCIAL 1\",\"RegistraCalificacion\":1,\"Fecha\":\"05/03/2021\",\"SituacionMateria\":\"AC\",\"Descripcion\":\"SEMINARIO DE INVESTIGACION I\",\"Opcion\":\"\",\"Ciclo\":\"08\",\"Tipo\":\"T\"},{\"Calificacion\":\"9.30\",\"Materia\":\"CI877\",\"Evaluacion\":\"PARCIAL 2\",\"RegistraCalificacion\":1,\"Fecha\":\"04/03/2021\",\"SituacionMateria\":\"AC\",\"Descripcion\":\"SEMINARIO DE INVESTIGACION I\",\"Opcion\":\"\",\"Ciclo\":\"08\",\"Tipo\":\"T\"},{\"Calificacion\":\"9.70\",\"Materia\":\"IA878\",\"Evaluacion\":\"PARCIAL 1\",\"RegistraCalificacion\":1,\"Fecha\":\"22/03/2021\",\"SituacionMateria\":\"AC\",\"Descripcion\":\"INGENIERIA DE SOFTWARE II\",\"Opcion\":\"\",\"Ciclo\":\"08\",\"Tipo\":\"T\"},{\"Calificacion\":\"9.20\",\"Materia\":\"IA878\",\"Evaluacion\":\"PARCIAL 2\",\"RegistraCalificacion\":1,\"Fecha\":\"27/04/2021\",\"SituacionMateria\":\"AC\",\"Descripcion\":\"INGENIERIA DE SOFTWARE II\",\"Opcion\":\"\",\"Ciclo\":\"08\",\"Tipo\":\"T\"},{\"Calificacion\":\"8.30\",\"Materia\":\"OC206\",\"Evaluacion\":\"PARCIAL 1\",\"RegistraCalificacion\":1,\"Fecha\":\"07/03/2021\",\"SituacionMateria\":\"AC\",\"Descripcion\":\"ADMINISTRACION\",\"Opcion\":\"\",\"Ciclo\":\"08\",\"Tipo\":\"T\"},{\"Calificacion\":\"10.00\",\"Materia\":\"OPC05\",\"Evaluacion\":\"PARCIAL 1\",\"RegistraCalificacion\":1,\"Fecha\":\"15/03/2021\",\"SituacionMateria\":\"AC\",\"Descripcion\":\"MOBILE PLATFORMS\",\"Opcion\":\"\",\"Ciclo\":\"08\",\"Tipo\":\"T\"}]"
        val itemType = object: TypeToken<List<Grade>>() {}.type
        val grades: List<Grade> = Gson().fromJson(json, itemType)
        val adapter = GradeAdapter(grades)

        val rvGrade = view.findViewById<RecyclerView>(R.id.rvGrades)
        rvGrade.adapter = adapter
        rvGrade.layoutManager = LinearLayoutManager(view.context)
    }
}