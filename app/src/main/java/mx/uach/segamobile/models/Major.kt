package mx.uach.segamobile.models

@Suppress("SpellCheckingInspection")
class Major (
    var UnidadAcademica: String,
    var DescProgramaEducativo: String,
    var Descripcion: String,
    var PlanEstudios: String,
    var Campus: String,
    var DescModalidad: String,
    var Estatus: String,
    var IdCicloEscolar: Int,
    var IdInscripcion: Int
){
}