package mx.uach.segamobile.models

class Grade (
    var Calificacion: String,
    var Materia: String,
    var Evaluacion: String,
    var RegistraCalificacion: Int,
    var Fecha: String,
    var Descripcion: String,
    var SituacionMateria: String,
    var Opcion: String,
    var Ciclo: String,
    var Tipo: String
) {
}