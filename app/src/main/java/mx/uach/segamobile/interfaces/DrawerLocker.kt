package mx.uach.segamobile.interfaces

interface DrawerLocker {
    fun lockDrawer()
    fun unlockDrawer()
}