package mx.uach.segamobile

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.navigation.NavigationView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_menu.*
import mx.uach.segamobile.adapters.MajorAdapter
import mx.uach.segamobile.models.Major

/**
 * A simple [Fragment] subclass.
 * Use the [MajorFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MajorFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_major, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val json: String = "[{\"ProgramaEducativoDescripcionCorta\":\"ICC\",\"IdEstatus\":4,\"Campus\":\"CHIHUAHUA\",\"ProgramaEducativo\":\"34\",\"IdModalidad\":1,\"IdPlanEstudios\":918,\"DescModalidad\":\"PRESENCIAL\",\"CveCampus\":\"00\",\"DescProgramaEducativo\":\"INGENIERO EN CIENCIAS DE LA COMPUTACION\",\"IddCampus\":808,\"IdUnidadPresupuestal\":\"4467\",\"DescNivelAcademico\":\"LICENCIATURA\",\"NivelAcademico\":\"03\",\"Descripcion\":\"SIN AREA/OPCION\",\"Estatus\":\"REINGRESO INSCRITO\",\"ClaveModalidad\":\"01\",\"PlanEstudios\":\"2016\",\"IdCampus\":1,\"IdNivelAcademico\":3,\"IddProgramaEducativo\":501,\"IdAreaOpcion\":0,\"IdProgramaEducativo\":486,\"IdUnidad\":\"4400\",\"IdCicloEscolar\":2411,\"IdInscripcion\":321801,\"UnidadAcademica\":\"FACULTAD DE INGENIERIA\"},{\"ProgramaEducativoDescripcionCorta\":\"MCP\",\"IdEstatus\":3,\"Campus\":\"CHIHUAHUA\",\"ProgramaEducativo\":\"01\",\"IdModalidad\":1,\"IdPlanEstudios\":758,\"DescModalidad\":\"PRESENCIAL\",\"CveCampus\":\"00\",\"DescProgramaEducativo\":\"MEDICO CIRUJANO Y PARTERO\",\"IddCampus\":119,\"IdUnidadPresupuestal\":\"5221\",\"DescNivelAcademico\":\"LICENCIATURA\",\"NivelAcademico\":\"03\",\"Descripcion\":\"SIN AREA/OPCION\",\"Estatus\":\"REINGRESO NO INSCRITO\",\"ClaveModalidad\":\"01\",\"PlanEstudios\":\"2013\",\"IdCampus\":1,\"IdNivelAcademico\":3,\"IddProgramaEducativo\":143,\"IdAreaOpcion\":0,\"IdProgramaEducativo\":129,\"IdUnidad\":\"5200\",\"IdCicloEscolar\":2370,\"IdInscripcion\":302121,\"UnidadAcademica\":\"FACULTAD DE MEDICINA Y CIENCIAS BIOMEDICAS\"}]"
        val itemType = object: TypeToken<List<Major>>() {}.type
        val majors: List<Major> = Gson().fromJson(json, itemType)
        val adapter = MajorAdapter(majors)

        val rvMajor = view.findViewById<RecyclerView>(R.id.rvMajors)
        rvMajor.adapter = adapter
        rvMajor.layoutManager = LinearLayoutManager(view.context)
    }
}