package mx.uach.segamobile.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import mx.uach.segamobile.R
import mx.uach.segamobile.models.Grade

class GradeAdapter(val grades: List<Grade>) : RecyclerView.Adapter<GradeAdapter.ViewHolder>() {
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        val tvCicloValue: TextView = listItemView.findViewById<TextView>(R.id.tvCicloValue)
        val tvMateriaValue = listItemView.findViewById<TextView>(R.id.tvMateriaValue)
        val tvDescripcionValue = listItemView.findViewById<TextView>(R.id.tvDescripcionValue)
        val tvTipoValue = listItemView.findViewById<TextView>(R.id.tvTipoValue)
        val tvEvaluacionValue = listItemView.findViewById<TextView>(R.id.tvEvaluacionValue)
        val tvCalificacionValue = listItemView.findViewById<TextView>(R.id.tvCalificacionValue)
        val tvFechaValue = listItemView.findViewById<TextView>(R.id.tvFechaValue)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val gradeView = inflater.inflate(R.layout.grade_item, parent, false)
        return ViewHolder(gradeView)
    }

    override fun getItemCount(): Int {
        return grades.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvCicloValue.text = grades.elementAt(position).Ciclo
        holder.tvMateriaValue.text = grades.elementAt(position).Materia
        holder.tvDescripcionValue.text = grades.elementAt(position).Descripcion
        holder.tvTipoValue.text = grades.elementAt(position).Tipo
        holder.tvEvaluacionValue.text = grades.elementAt(position).Evaluacion
        holder.tvCalificacionValue.text = grades.elementAt(position).Calificacion
        holder.tvFechaValue.text = grades.elementAt(position).Fecha
    }
}