package mx.uach.segamobile.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import mx.uach.segamobile.R
import mx.uach.segamobile.models.Major

class MajorAdapter(val majors: List<Major>) : RecyclerView.Adapter<MajorAdapter.ViewHolder>() {
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        val vMajorItem = listItemView.findViewById<View>(R.id.vMajorItem)
        val tvUnidadAcademica = listItemView.findViewById<TextView>(R.id.tvUnidadAcademicaValue)
        val tvProgramaEducativo = listItemView.findViewById<TextView>(R.id.tvProgramaEducativoValue)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val majorView = inflater.inflate(R.layout.major_item, parent, false)
        return ViewHolder(majorView)
    }

    override fun getItemCount(): Int {
        return majors.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvUnidadAcademica.text = majors.elementAt(position).UnidadAcademica
        holder.tvProgramaEducativo.text = majors.elementAt(position).DescProgramaEducativo
        holder.vMajorItem.setOnClickListener(View.OnClickListener {
            Navigation.findNavController(holder.vMajorItem).navigate(R.id.menuFragment)
        })
    }
}